// 1. Потому что существует риск перехвата данных

let wrapper = document.querySelector('.btn-wrapper')
document.addEventListener('keydown', buttonHighlight)

function buttonHighlight (e) {
    let button = buttonCompare(e.key.toUpperCase())
    removeActive()
    addActive(button)
}

function buttonCompare (item) {
 let find = [...wrapper.children].find((e) => e.textContent.toUpperCase() === item)
    return find
}

function removeActive () {
    [...wrapper.children].forEach(e => e.classList.remove('active'))
}

function addActive (elem) {
    elem.classList.add('active')
}


